package com.streams.samples.fraud.core;

import com.streams.samples.fraud.common.Money;
import com.streams.samples.fraud.model.FraudCheckCompleted;
import com.streams.samples.fraud.model.FraudCheckResultStatus;
import com.streams.samples.fraud.model.FraudCommandSignal;
import com.streams.samples.fraud.model.PaymentReceived;
import io.vavr.API;
import io.vavr.Tuple2;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class FraudChecker {
    public FraudCheckCompleted checkPayment(Tuple2<PaymentReceived, FraudCommandSignal> paymentAndSignalTuple) {
        return new FraudCheckCompleted(paymentAndSignalTuple._1.getPaymentId(), fraudCheckResultStatus(paymentAndSignalTuple));
    }

    private FraudCheckResultStatus fraudCheckResultStatus(Tuple2<PaymentReceived, FraudCommandSignal> paymentAndSignalTuple) {
        var paymentAmount = Money.valueOf(paymentAndSignalTuple._1.getAmount());

        if ( paymentAmount.compareTo(Money.valueOf("500")) < 0 ) {
            return FraudCheckResultStatus.Accepted;
        } else if ( paymentAmount.compareTo(Money.valueOf("1000")) < 0 ) {
            return FraudCheckResultStatus.Hold;
        }

        return FraudCheckResultStatus.Rejected;
    }
}
