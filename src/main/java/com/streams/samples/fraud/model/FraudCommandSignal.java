package com.streams.samples.fraud.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FraudCommandSignal {
    private String commandPayload;
    private ProcessingType processingType = ProcessingType.FastPayment;

    public FraudCommandSignal() {
    }

    public FraudCommandSignal(String commandPayload) {
        this.commandPayload = commandPayload;
    }

    public ProcessingType getProcessingType() {
        return processingType;
    }

    public void setProcessingType(ProcessingType processingType) {
        this.processingType = processingType;
    }

    public String getCommandPayload() {
        return commandPayload;
    }

    public void setCommandPayload(String commandPayload) {
        this.commandPayload = commandPayload;
    }
}
