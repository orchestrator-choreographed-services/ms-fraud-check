package com.streams.samples.fraud.model;

public enum FraudCheckResultStatus {
    Accepted,
    Hold,
    Rejected;
}
