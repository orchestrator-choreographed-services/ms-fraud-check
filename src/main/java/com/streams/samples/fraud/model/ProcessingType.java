package com.streams.samples.fraud.model;

public enum ProcessingType {
    FastPayment,
    DowngradedPayment;
}
