package com.streams.samples.fraud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;

@SpringBootApplication
@EnableSchemaRegistryClient
public class FraudCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(FraudCheckApplication.class, args);
	}

}
