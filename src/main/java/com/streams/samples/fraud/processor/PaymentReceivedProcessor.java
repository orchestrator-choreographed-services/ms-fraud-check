package com.streams.samples.fraud.processor;

import com.streams.samples.fraud.core.FraudChecker;
import com.streams.samples.fraud.model.FraudCheckCompleted;
import com.streams.samples.fraud.model.FraudCommandSignal;
import com.streams.samples.fraud.model.PaymentReceived;
import com.streams.samples.fraud.model.ProcessingType;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Slf4j
@EnableBinding(PaymentReceivedProcessor.StreamProcessor.class)
public class PaymentReceivedProcessor {
    @Autowired
    private FraudChecker fraudChecker;

    @StreamListener
    @SendTo("fraudCheckCompleted")
    public KStream<String, FraudCheckCompleted> process(@Input("paymentReceived") KStream<String, PaymentReceived> paymentReceived,
                                                        @Input("fraudCommandSignal")KStream<String, FraudCommandSignal> commandSignal) {
        paymentReceived.peek((k, v) -> {
            log.info("Received a payment created event with payment ID {}", k);
        });

        commandSignal.peek((k, v) -> {
            log.info("Received a command signal for payment ID {}", k);
        });


        var branchedCommandSignalStreams = branchOutProcessingTypeStreams(commandSignal);

        var fastPaymentFraudCheckCompletedStream = processFastPayments(paymentReceived, branchedCommandSignalStreams._1);
        var downgradedPaymentFraudCheckCompletedStream = processDowngradedPayments(paymentReceived, branchedCommandSignalStreams._2);

        return fastPaymentFraudCheckCompletedStream.merge(downgradedPaymentFraudCheckCompletedStream);
    }

    private KStream<String, FraudCheckCompleted> processFastPayments(KStream<String, PaymentReceived> paymentReceived,
                                                                     KStream<String, FraudCommandSignal> commandSignal) {
        var joinedPaymentsToRunFraudAgainst = paymentReceived.join(commandSignal,
                Tuple::of,
                joinWindow(),
                streamJoinedSerdes()
        );

        //At this point the fraud can do its work as it gets the payment and also the signal to start.
        joinedPaymentsToRunFraudAgainst.peek((k, v) -> log.info("Now I can do fraud checking for a fast payment with ID {}", k));

        return joinedPaymentsToRunFraudAgainst.mapValues(fraudChecker::checkPayment);
    }

    private KStream<String, FraudCheckCompleted> processDowngradedPayments(KStream<String, PaymentReceived> paymentReceived,
                                                                     KStream<String, FraudCommandSignal> commandSignal) {

        var joinedPaymentsToRunFraudAgainst = paymentReceived.join(commandSignal,
                Tuple::of,
                joinWindow(),
                streamJoinedSerdes());

        //At this point the fraud can do its work as it gets the payment and also the signal to start.
        joinedPaymentsToRunFraudAgainst.peek((k, v) -> log.info("-----Now I can do fraud checking for a DOWNGRADED payment with ID {}-----", k));

        return joinedPaymentsToRunFraudAgainst.mapValues(fraudChecker::checkPayment);
    }

    private Joined<String, PaymentReceived, FraudCommandSignal> streamJoinedSerdes() {
        return Joined.with(new Serdes.StringSerde(), new JsonSerde<>(PaymentReceived.class),
                new JsonSerde<>(FraudCommandSignal.class));
    }

    private JoinWindows joinWindow() {
        return JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());
    }

    private Tuple2<KStream<String, FraudCommandSignal>, KStream<String, FraudCommandSignal>> branchOutProcessingTypeStreams(@Input("fraudCommandSignal") KStream<String, FraudCommandSignal> commandSignal) {
        var branchedStreams = commandSignal.branch((k, v) -> v.getProcessingType() == ProcessingType.FastPayment,
                (k, v) -> v.getProcessingType() == ProcessingType.DowngradedPayment);

        return Tuple.of(branchedStreams[0], branchedStreams[1]);
    }

    public interface StreamProcessor {
        @Input("paymentReceived")
        KStream<?, ?> paymentReceived();

        @Input("fraudCommandSignal")
        KStream<?, ?> fraudCommandSignal();

        @Output("fraudCheckCompleted")
        KStream<?, ?> fraudCheckCompleted();
    }
}
