package com.streams.samples.fraud.common;

public enum Currency {
    AUD,
    USD,
    EGP
}
