# Fraud check

This service checks if the payment is fraud or not. 
For the purpose of testing the following table shows the expected fraud state.

*Please note* this might contradict with [Limit check rules](https://gitlab.com/orchestrator-choreographed-services/ms-limit-check)


| Amount        | Fraud State |
| ------------- |:-------------:|
| From 0 to 500      | Accepted |
| From 500 to 1000      | Hold      |
| Above 1000 | Rejected      |

## Signal types

Fraud is supporting 2 different types of processing, the type will depend on the command-signal sent by workflow engine.
So the same `PaymentReceived` will be joined with command-signal events consumed from topic `tw-payments.command-signal.fraud-check`

* Fast payment
* Downgraded payment 

## Prerequisite

Before running this service please make sure you have the following

* Start all required services by following the steps in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure)
* Make sure the required topics are pre-created in the Kafka cluster you are using

## Start the service

To start the service, you could use `gradle bootrun`.

## Publish payments

To publish PaymentCreated event, please use the scripts in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure/blob/master/publish_payments.sh)

## Publish command signal

To publish command signal to fraud topic you could use [publish command signal script](https://gitlab.com/orchestrator-choreographed-services/infrastructure/blob/master/publish_command-signal.sh) 

## Future work

* De-duplicate payments

Given that we are joining `PaymentReceived` with `command-signal` event, 
this might cause fraud to run twice if the same payment is received 2 times.
The first time will be blocked waiting for the command-signal, once it arrives 
the join will happen and `FraudCheckCompleted` will be published. 
Later if the same payment is sent(may be because downstream service send duplicates), the join will happen immediately
because the `command-signal` was already received before and the join-window is still valid.
We might need to support de-duplication layer in front of fraud processor. 
So if the payment was processed before by fraud, all next payments of the same key will be ignored.
This will not stop us from running fraud multiple times for different workflow, 
simply because each time we need to run fraud, we will send different signals like fastPayment and downgradedPayment.  

